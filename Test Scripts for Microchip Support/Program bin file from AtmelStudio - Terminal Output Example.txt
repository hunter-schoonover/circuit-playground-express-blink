Welcome to the Skoonie Serial ARM Programmer This tool can be used to program ARM powered Arduino boards via the serial bootloader using bossac.

If you have not done so already, edit the config file. The config file can be found here:
     C:\Users\Hunter\Documents\Work\Venom\Development\Circuit Playground Express Testing\Blink 0611201428 ~ Blink from Arduino Sketch\Test Scripts for Microchip Support\SkoonieSerialARMProgrammer\00_config.ini

Also be sure that you have added the following line to the Arguments parameter of your External Tool in AtmelStudio:
     $(ProjectDir)Debug\$(TargetName).bin

Here is a list of all the serial devices connected to COM ports on this computer:
     USB Serial Device (COM4)

Which COM port is the Arduino device on? Answer with the COM port's number ("1", "2", "3", etc.): 4

Forcing reset using 1200bps open/close on port COM4

Status for device COM4:
-----------------------
    Baud:            115200
    Parity:          None
    Data Bits:       8
    Stop Bits:       1
    Timeout:         OFF
    XON/XOFF:        OFF
    CTS handshaking: OFF
    DSR handshaking: OFF
    DSR sensitivity: OFF
    DTR circuit:     OFF
    RTS circuit:     ON

Delaying 5 seconds to allow device to reset.

Programming device...

If program says device cannot be found, restart this program.
If program freezes at "Set binary mode", unplug device from
     computer and plug it back in. Then restart this program.
If program says "No such file or directory" either...
     1. The path to the compiled binary file provided in the
          Arguments for the AtmelStudio External Tool is not
          the right path.
     2. The project has not been built/compiled yet.
     3. A file from another project was open when this program was run.


C:\Users\Hunter\Documents\Work\Venom\Development\Circuit Playground Express Testing\Blink 0611201428 ~ Blink from Arduino Sketch\Test Scripts for Microchip Support\SkoonieSerialARMProgrammer>call "bossac\1.7.0-arduino3\bossac.exe" -i -d --port=COM4 -U true -i -e -w -v  "../../Blink 0611201428/Blink/Debug/Blink.bin" -R
Set binary mode
readWord(addr=0)=0x20002dd8
readWord(addr=0xe000ed00)=0x410cc601
readWord(addr=0x41002018)=0x10010305
version()=v1.1 [Arduino:XYZ] May  8 2018 12:07:33
chipId=0x10010005
Connected at 921600 baud
readWord(addr=0)=0x20002dd8
readWord(addr=0xe000ed00)=0x410cc601
readWord(addr=0x41002018)=0x10010305
Atmel SMART device 0x10010005 found
write(addr=0x20004000,size=0x34)
writeWord(addr=0x20004030,value=0x10)
writeWord(addr=0x20004020,value=0x20008000)
Device       : ATSAMD21G18A
readWord(addr=0)=0x20002dd8
readWord(addr=0xe000ed00)=0x410cc601
readWord(addr=0x41002018)=0x10010305
Chip ID      : 10010005
version()=v1.1 [Arduino:XYZ] May  8 2018 12:07:33
Version      : v1.1 [Arduino:XYZ] May  8 2018 12:07:33
Address      : 8192
Pages        : 3968
Page Size    : 64 bytes
Total Size   : 248KB
Planes       : 1
Lock Regions : 16
Locked       : readWord(addr=0x41004020)=0xffff
readWord(addr=0x41004020)=0xffff
readWord(addr=0x41004020)=0xffff
readWord(addr=0x41004020)=0xffff
readWord(addr=0x41004020)=0xffff
readWord(addr=0x41004020)=0xffff
readWord(addr=0x41004020)=0xffff
readWord(addr=0x41004020)=0xffff
readWord(addr=0x41004020)=0xffff
readWord(addr=0x41004020)=0xffff
readWord(addr=0x41004020)=0xffff
readWord(addr=0x41004020)=0xffff
readWord(addr=0x41004020)=0xffff
readWord(addr=0x41004020)=0xffff
readWord(addr=0x41004020)=0xffff
readWord(addr=0x41004020)=0xffff
none
readWord(addr=0x41004018)=0
Security     : false
Boot Flash   : true
readWord(addr=0x40000834)=0x7000a
BOD          : true
readWord(addr=0x40000834)=0x7000a
BOR          : true
Arduino      : FAST_CHIP_ERASE
Arduino      : FAST_MULTI_PAGE_WRITE
Arduino      : CAN_CHECKSUM_MEMORY_BUFFER
Erase flash
chipErase(addr=0x2000)
done in 0.874 seconds

Write 11272 bytes to flash (177 pages)
write(addr=0x20005000,size=0x1000)
writeBuffer(scr_addr=0x20005000, dst_addr=0x2000, size=0x1000)
[==========                    ] 36% (64/177 pages)write(addr=0x20005000,size=0x1000)
writeBuffer(scr_addr=0x20005000, dst_addr=0x3000, size=0x1000)
[=====================         ] 72% (128/177 pages)write(addr=0x20005000,size=0xc40)
writeBuffer(scr_addr=0x20005000, dst_addr=0x4000, size=0xc40)
[==============================] 100% (177/177 pages)
done in 0.145 seconds

Verify 11272 bytes of flash with checksum.
checksumBuffer(start_addr=0x2000, size=0x1000) = 76eb
checksumBuffer(start_addr=0x3000, size=0x1000) = 7ca6
checksumBuffer(start_addr=0x4000, size=0xc08) = 8365
Verify successful
done in 0.041 seconds
CPU reset.
readWord(addr=0)=0x20002dd8
readWord(addr=0xe000ed00)=0x410cc601
readWord(addr=0x41002018)=0x10010305
writeWord(addr=0xe000ed0c,value=0x5fa0004)


Program finished.
Press any key to continue . . .