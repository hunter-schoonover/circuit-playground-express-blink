@echo off

REM *******************************************************************************
REM Title: SkoonieSerialARMProgrammer.bat
REM Author: Hunter Schoonover
REM Version: 1-0
REM Last Edit Date: 06/09/2020
REM
REM Purpose:
REM
REM Programs an Arduino board using the Serial Bootloader.
REM

setlocal enabledelayedexpansion

REM ------------------------------------------------------------------------------
REM ::Declare Variables

set pathToThisBatchFile=%~dp0

REM Get path from passed in argument
set pathToCompiledBinaryFile=%1

REM Get config settings and create directories
set scriptReadFromIni=%pathToThisBatchFile%readFromIni.bat
set pathToIniConfig=%pathToThisBatchFile%01_config.ini

REM ------------------------------------------------------------------------------
REM ::Load Settings from Config

REM Load setting from config
set func_param0=displayWelcomeMessage
call :readFromIni
set shouldDisplayWelcomeMessage=%func_return0%

REM Load setting from config
set func_param0=pathToBossacExe
call :readFromIni
set pathToBossacExe=%func_return0%

REM ------------------------------------------------------------------------------
REM ::Begin Program

REM Change to batch file directory
cd "%pathToThisBatchFile%"

REM Display message welcome if config says to
if %shouldDisplayWelcomeMessage%==true call ::displayWelcomeMessage

echo.
echo Here is a list of all the serial devices connected to COM ports on this computer:
for /f "tokens=1* delims==" %%I in ('wmic path win32_pnpentity get caption /format:list ^| find "COM"') do (
    echo.     %%~J
)

echo.
set /p comPortNumber=Which COM port is the Arduino device on? Answer with the COM port's number ("1", "2", "3", etc.): 

echo.
echo Forcing reset using 1200bps open/close on port COM%comPortNumber%

REM Forcing reset using 1200bps open/close
mode COM%comPortNumber%: BAUD=1200 parity=N data=8 stop=1

REM Delay to ensure device has time to reset
echo Delaying 5 seconds to allow device to reset.
PING localhost -n 6 >NUL

echo.
echo Programming device... 

echo.
echo If program says device cannot be found, restart this program.
echo If program freezes at "Set binary mode", unplug device from 
echo.     computer and plug it back in. Then restart this program.
echo If program says "No such file or directory" either... 
echo.     1. The path to the compiled binary file provided in the 
echo.          Arguments for the AtmelStudio External Tool is not 
echo.          the right path.
echo.     2. The project has not been built/compiled yet.
echo.     3. A file from another project was open when this program was run.
echo.

@REM Program device
@echo on
call "%pathToBossacExe%" -i -d --port=COM%comPortNumber% -U true -i -e -w -v  %pathToCompiledBinaryFile% -R
@echo off

echo.
echo.
echo Program finished.

pause
exit 0

REM ------------------------------------------------------------------------------
REM ::readFromIni
REM
REM Reads the value associated with the passed in key from the ini file.
REM
REM @param 	func_param0		Key for value
REM @return func_return0	Value
REM

:readFromIni

	REM Get value
	for /f "tokens=* USEBACKQ" %%F in (`call "%scriptReadFromIni%" "%pathToIniConfig%" Settings %func_param0%`) do ( set func_return0=%%F )

	REM Remove trailing and leading spaces
	for /l %%a in (1,1,100) do if "!func_return0:~-1!"==" " set func_return0=!func_return0:~0,-1!
	
	exit /B 0
	
REM end of ::readFromIni
REM ------------------------------------------------------------------------------

REM ------------------------------------------------------------------------------
REM ::displayWelcomeMessage
REM
REM Echos the welcome message to the console.
REM

:displayWelcomeMessage

	echo Welcome to the Skoonie Serial ARM Programmer! This tool can be used to program ARM powered Arduino boards via the serial bootloader using bossac.

	echo.
	echo If you have not done so already, edit the config file. The config file can be found here:
	echo.     %pathToThisBatchFile%00_config.ini
	
	echo.
	echo Also be sure that you have added the following line to the Arguments parameter of your External Tool in AtmelStudio:
	echo.     $(ProjectDir)Debug\$(TargetName).bin
	
	exit /B 0
	
REM end of ::displayWelcomeMessage
REM ------------------------------------------------------------------------------