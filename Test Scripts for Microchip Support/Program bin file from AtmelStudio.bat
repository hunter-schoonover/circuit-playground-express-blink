@echo off

setlocal enabledelayedexpansion

REM ------------------------------------------------------------------------------
REM ::Declare Variables

REM ------------------------------------------------------------------------------
REM ::Begin Program

REM Program device using binary file compiled by Arduino IDE
call "SkoonieSerialARMProgrammer/SkoonieSerialARMProgrammer.bat" "../../Blink 0611201428/Blink/Debug/Blink.bin"

REM exit 0